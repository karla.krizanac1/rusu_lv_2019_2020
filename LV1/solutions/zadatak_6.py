import re

filename = input("Enter filename: ")
file = open(filename)

lst = []

for line in file:
    if line.startswith("From"):
        match = re.search(r'[\w\.-]+@[\w\.-]+', line)
        lst.append(match.group(0))

dic = dict()
for k in lst:
    domain = k.split('@')[1]
    if domain not in dic:
        dic[domain] = 1
    else:
        dic[domain] +=1

print(dic)
