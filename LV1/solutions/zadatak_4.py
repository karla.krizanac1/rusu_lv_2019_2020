list_numbers = []

while(True):
    x = input("Enter a value: ")
    if x == "Done":
        break
    try:
        list_numbers.append(float(x))
    except ValueError:
        print("Not a number.")
        
print("List size: ", len(list_numbers))
print("List min: ", min(list_numbers))
print("List max: ", max(list_numbers))
print("List average: ", sum(list_numbers)/len(list_numbers))
